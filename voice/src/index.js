'use strict';

var APP_ID = 'amzn1.ask.skill.d1a76408-a5f9-42f3-ad35-911db1e6b489';
var Alexa = require('alexa-sdk');
const doc = require('dynamodb-doc');
const dynamo = new doc.DynamoDB();


var STATES = {
    MAIN: '_MAINMODE',
    INGREDIENTS: '_INGREDIENTSMODE',
    DIRECTIONS: '_DIRECTIONSMODE'
};

var languageString = {
    'en': {
        'translation': {
            'WELCOME_MESSAGE' : "welcome to recipe assistant, ",
            'MAIN_PROMPT' : "what recipe would you like to make?",
            'MAIN_HELP' : "search for a recipe by asking, 'find recipe'. list available recipes by saying, 'list recipes'.",
            'NO_RECIPE' : "please specify some arguments for your search",
            'TRY_AGAIN' : "sorry, but i couldn't find that recipe, please try again",
            'TRY_SEARCH': "sorry, but i have no record of a current recipe, please search for a recipe",
            'INGREDIENTS_FIRST' : "first ingredient: ",
            'INGREDIENTS_PREFACE' : "loading the ingredients for %s. ",
            'DIRECTIONS_FIRST' : "first step: ",
            'DIRECTIONS_PREFACE' : "loading direction to make %s. ",
            'INGREDIENTS_HELP' : "you can say 'next item', 'last item', 'go to directions', 'main menu', or 'exit'",
            'DIRECTIONS_HELP' : "you can say 'next item', 'last item', 'go to ingredients', 'main menu', or 'exit'",
            'LAST_INGREDIENT': "last ingredient: ",
            'NEXT_INGREDIENT': "next ingredient: ",
            'LAST_DIRECTION': "last step: ",
            'NEXT_DIRECTION': "next step: ",
            'SAY_NEXT': "say next to go to the next ingredient",
            'DONE': "recipe directions complete, returning to main menu",
            'EXIT': "exiting recipe assistant",
            'RECIPES': 'init'
        }
    }
};

exports.handler = function (event, context, callback) {

    const skillRequest = function(err, res) {
        var alexa = Alexa.handler(event, context);
        languageString['en']['translation']['RECIPES'] = res['Items'];
        alexa.appId = APP_ID;
        alexa.resources = languageString;
        alexa.registerHandlers(newSessionHandlers, mainStateHandlers, ingredientsStateHandlers, directionStateHandlers);
        alexa.execute();
    };

    /** Code below adapted from dynamo_api.js file given to us on Piazza. */

    const httpRequest = function(err, res) {callback(null, {
        statusCode: err ? '400' : '200',
        body: err ? err.message : JSON.stringify(res),
        headers: {
            'Access-Control-Allow-Headers': 'x-Requested-With',
            'Access-Control-Allow-Origin': '*',
            "Access-Control-Allow-Credentials" : true,
            'Content-Type': 'application/json'
        }
    })};

    if ('request' in event) {
        dynamo.scan({TableName: 'Recipes'}, skillRequest);
    } else if ('httpMethod' in event) {
        switch (event.httpMethod) {
            case 'GET':
                dynamo.scan({ TableName: event.queryStringParameters.TableName }, httpRequest);
                break;
            case 'POST':
                var json = JSON.parse(event.body);
                if (json["type"] == 'POST') {
                    dynamo.putItem(json["data"], httpRequest);
                }
                else if (json["type"] == 'PUT') {
                    dynamo.updateItem(json["data"], httpRequest);
                }
                else if (json["type"] == 'DELETE') {
                    dynamo.deleteItem(json["data"], httpRequest);
                }
                else if (json["type"] == 'QUERY') {
                    dynamo.scan(json["data"], httpRequest);
                } else{
                    httpRequest(new Error("Unsupported method"));
                }
                break;
            default:
                httpRequest(new Error("Unsupported method"));
        }
    }

};

var newSessionHandlers = {
    'LaunchRequest': function () {
        this.handler.state = STATES.MAIN;
        this.emitWithState('start', true);
    },
    
    'Unhandled': function () {
        this.emit('LaunchRequest');
    }
};

var mainStateHandlers = Alexa.CreateStateHandler(STATES.MAIN, {
    'start': function (newSession) {
        var speechOutput = this.t('MAIN_PROMPT');
        if (newSession) {
            speechOutput = this.t('WELCOME_MESSAGE') + speechOutput;
        }
        this.emit(':ask', speechOutput);
    },

    'SearchIntent': function () {
        var recipeSlot = this.event.request.intent.slots.Recipe;
        if (recipeSlot && recipeSlot.value) {
            var recipeName = recipeSlot.value.toLowerCase();
        } else {
            this.emit(':ask', this.t('NO_RECIPE'));
        }
        var recipes = this.t('RECIPES');
        var recipe = '';
        for (var i = 0; i < recipes.length; i++) {
            if (recipes[i]['RecipeName'].toLowerCase() == recipeName) {
                recipe = recipes[i];
                break;
            }
        }
        if (recipe == '') {
            this.emit(':ask', this.t('TRY_AGAIN'));
        } else {
            recipe['Ingredients'] = recipe['Ingredients'].split('\n');
            recipe['Directions'] = recipe['Directions'].split('\n');
            this.attributes['currentRecipe'] = recipe;
            this.handler.state = STATES.INGREDIENTS;
            this.emitWithState('start');
        }
    },

    'IngredientsIntent': function () {
        if (this.attributes['currentRecipe'] != null) {
            this.handler.state = STATES.INGREDIENTS;
            this.emitWithState('StartOverIntent', false);
        } else {
            this.emit(':ask', this.t('TRY_SEARCH'));
        }

    },

    'DirectionsIntent': function () {
        if (this.attributes['currentRecipe'] != null) {
            this.handler.state = STATES.DIRECTIONS;
            this.emitWithState('StartOverIntent', false);
        } else {
            this.emit(':ask', this.t('TRY_SEARCH'));
        }
    },

    'RecipeIntent': function () {
        var speechOutput = '';
        var recipes = this.t('RECIPES');
        for (var i = 0; i < recipes.length; i++) {
            var recipe = recipes[i];
            speechOutput += recipe['RecipeName'] + ', ';
        }
        this.emit(':ask', speechOutput);
    },

    'HelpIntent': function () {
        this.emit(':ask', this.t('MAIN_HELP'));
    },

    'ExitIntent': function () {
        this.emitWithState(':tell', this.t('EXIT'));
    },

    'Unhandled': function () {
        this.emitWithState('HelpIntent');
    }

});

var ingredientsStateHandlers = Alexa.CreateStateHandler(STATES.INGREDIENTS, {
    'start': function () {
        this.emitWithState('StartOverIntent', true);
    },

    'NextIntent': function () {
        this.attributes['currentIngredient'] += 1;
        if (this.attributes['currentRecipe']['Ingredients'].length == this.attributes['currentIngredient']) {
            this.handler.state = STATES.DIRECTIONS;
            this.emitWithState('start');
        } else {
            this.emitWithState('readIngredient', this.t('NEXT_INGREDIENT'));
        }
    },

    'StartOverIntent': function (newSession) {
        var speechOutput = '';
        if (newSession) {
            var recipeName = this.attributes['currentRecipe']['RecipeName'];
            speechOutput += this.t('INGREDIENTS_PREFACE', recipeName);
        }
        this.attributes['currentIngredient'] = 0;
        this.emitWithState('readIngredient', speechOutput + this.t('INGREDIENTS_FIRST'));
    },

    'LastIntent': function () {
        if (this.attributes['currentIngredient'] > 0) {
            this.attributes['currentIngredient'] -= 1;
        }
        this.emitWithState('readIngredient', this.t('LAST_INGREDIENT'));

    },

    'readIngredient': function (speechOutput) {
        speechOutput += this.attributes['currentRecipe']['Ingredients'][this.attributes['currentIngredient']];
        this.emit(':ask', speechOutput, this.t('SAY_NEXT'));
    },

    'MainMenuIntent': function () {
        this.handler.state = STATES.MAIN;
        this.emitWithState('start', false);
    },

    'DirectionsIntent': function () {
        this.handler.state = STATES.DIRECTIONS;
        this.emitWithState('StartOverIntent', false);
    },

    'HelpIntent': function () {
        this.emit(':ask', this.t('INGREDIENTS_HELP'));
    },

    'ExitIntent': function () {
        this.emitWithState(':tell', this.t('EXIT'));
    },

    'Unhandled': function () {
        this.emitWithState('HelpIntent');
    }

});


var directionStateHandlers = Alexa.CreateStateHandler(STATES.DIRECTIONS, {
    'start': function () {
        this.emitWithState('StartOverIntent', true);
    },

    'NextIntent': function () {
        this.attributes['currentDirection'] += 1;
        if (this.attributes['currentRecipe']['Directions'].length == this.attributes['currentDirection']) {
            this.handler.state = STATES.MAIN;
            this.emit(':ask', this.t('DONE'));
        } else {
            this.emitWithState('readDirection', this.t('NEXT_DIRECTION'));
        }
    },

    'StartOverIntent': function (newSession) {
        var speechOutput = '';
        if (newSession) {
            var recipeName = this.attributes['currentRecipe']['RecipeName'];
            speechOutput += this.t('DIRECTIONS_PREFACE', recipeName);
        }
        this.attributes['currentDirection'] = 0;
        this.emitWithState('readDirection', speechOutput + this.t('DIRECTIONS_FIRST'));
    },

    'LastIntent': function () {
        this.attributes['currentDirection'] -= 1;
        this.emitWithState('readDirection', this.t('LAST_DIRECTION'));
    },

    'readDirection': function (speechOutput) {
        speechOutput += this.attributes['currentRecipe']['Directions'][this.attributes['currentDirection']];
        this.emit(':ask', speechOutput, this.t('SAY_NEXT'));
    },

    'MainMenuIntent': function () {
        this.handler.state = STATES.MAIN;
        this.emitWithState('start', false);
    },

    'IngredientsIntent': function () {
        this.handler.state = STATES.INGREDIENTS;
        this.emitWithState('StartOverIntent', false);
    },

    'HelpIntent': function () {
        this.emit(':ask', this.t('DIRECTIONS_HELP'));
    },

    'ExitIntent': function () {
        this.emitWithState(':tell', this.t('EXIT'));
    },

    'Unhandled': function () {
        this.emitWithState('HelpIntent');
    }

});
